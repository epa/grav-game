from distutils.core import setup
import os
import py2exe

"""
1) OPTIONAL: set virtualenv
2) pip install -r requirements-dev.txt
3) Run "python setup.py py2exe"
"""

from_img_folder = 'imgs/'
to_img_folder = 'imgs'

data_files = []
for files in os.listdir(from_img_folder):
    f1 = from_img_folder + files
    if os.path.isfile(f1):  # skip directories
        f2 = to_img_folder, [f1]
        data_files.append(f2)


setup_kwargs = {
    'name': 'grav',
    'version': '0.1',
    'console': ['grav.py'],
    'data_files': data_files,
    'options': {
        'py2exe': {
            'optimize': 2,
            'bundle_files': 1,
            'unbuffered': True,
            'compressed': True
        },

    },
    'windows': [{
        'script': 'grav.py',
        'icon_resources': [(1, 'imgs/grav.ico')],
        'dest_base': 'grav'
    }],
    'zipfile': None,
}
# Run setup twice
# Without this icon wont show in the exe file
# Hack from https://stackoverflow.com/questions/29007820/exe-icon-doesnt-change-py2exe/36116599#36116599
# The tempfile method will leave a mess...
setup(**setup_kwargs)
setup(**setup_kwargs)
