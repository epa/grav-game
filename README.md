# Grav

- This was the first coding/game project of mine. (I just cleaned it a bit and got it running again.)
- Game is 1 vs 1.


## Controls

| Action             | Player 1  | Player 2  |
|--------------------|-----------|-----------|
| Move Up            | W         | Numpad 8  |
| Move Down          | S         | Numpad 5  |
| Move Left          | A         | Numpad 4  |
| Move Right         | D         | Numpad 6  |
| Energy Ball Attack | space     | Numpad 0  |
| Lightning Attack*  | Q         | Numpad 7  |
| Bomb Attack        | E         | Numpad 9  |
| Maul Attack        | F         | Numpad 3  |
| Change Avatar      | N         | M         |


\* Press down to reload lightning. Keep pressed until lightning symbol grows.
Release and press again to shoot the lightning.


## Setup

### Setup from code
- Install python 2.7
- `pip install -r requirements.txt`
- `python grap.py`

### Run from exe (Windows only)
- run `dist\grav.exe`

### Create exe
- `pip install -r requirements-dev.txt`
- `python setup.py py2exe`

### Install from msi (Windows only)
- run `dist\grav-0.1-win32.msi`

### Create msi
- `pip install -r requirements-dev-cx.freeze.txt`
- `python setup_cx_freeze.py bdist_msi`

