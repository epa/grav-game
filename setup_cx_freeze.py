import os
from cx_Freeze import setup, Executable

"""
1) OPTIONAL: set virtualenv
2) pip install -r requirements-dev-cx-freeze.txt
3) CREATE INSTALLER
python setup_cx_freeze.py bdist_msi
"""

from_img_folder = 'imgs/'
to_img_folder = 'imgs'

data_files = []
include_files = []
for files in os.listdir(from_img_folder):
    f1 = from_img_folder + files
    if os.path.isfile(f1):  # skip directories
        f2 = to_img_folder, [f1]
        data_files.append(f2)
        include_files.append((f1, to_img_folder + '/' + files))

setup(
    name='grav',
    version='0.1',
    # console=['grav.py'],
    data_files=data_files,
    options={
        'build_exe': {
            # Dependencies are automatically detected, but it might need fine tuning.
            'packages': ["os", "sys", 'pygame'],
            'include_files': include_files,
            'include_msvcr': True,
        }
    },
    executables=[
        Executable(
            script='grav.py',
            base='Win32GUI',
            icon='imgs/grav.ico',
        )
    ]
)
