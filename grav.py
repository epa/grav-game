# -*- coding: cp1252 -*-
import math
import time

import os
import pygame
import sys
from pygame.locals import *

# square off

t1 = 0.01

mm = 110
y1 = 540

x = 20

y2 = 540
x2 = 785

v = 0
a = 0
dd = 0

ttt = 0
ttt1 = 0
sn = 0
aikam = 0

ssss = 0

xk = 820
yk = 580

salama_vakio = 0
salama_vakio2 = 0

isku_v1 = 0
isku_v2 = 0

img_folder = 'imgs/'
if not os.path.isdir(img_folder):
    # if the imgs are not in separate imgs folder
    # (e.g. due to exporting)
    img_folder = ''

tsta = img_folder + "tausta3.jpg"
nlio = img_folder + "nelio.jpg"
nlio2 = img_folder + "nelio2.jpg"
nlio3 = img_folder + "nelio3.png"
nlio4 = img_folder + "nelio4.png"
nlio5 = img_folder + "nelio5.png"
nlio6 = img_folder + "nelio6.png"
nlio7 = img_folder + "nelio7.png"
nlio8_0 = img_folder + "nelio8.png"
nlio8_1 = img_folder + "nelio8_1.png"
nlio8_2 = img_folder + "nelio8_2.png"
nlio8_3 = img_folder + "nelio8_3.png"
nlio8_4 = img_folder + "nelio8_4.png"
nlio8_5 = img_folder + "nelio8_5.png"
nlio9 = img_folder + "nelio9.png"
nlio10 = img_folder + "nelio10.png"
nlio11 = img_folder + "nelio11.png"

plo1 = img_folder + "pallo1.png"
plo2 = img_folder + "pallo2.png"
plo3 = img_folder + "pallo3.png"
plo4 = img_folder + "pallo4.png"
plo5 = img_folder + "pallo5.png"
plo6 = img_folder + "pallo6.png"
plo7 = img_folder + "pallo7.png"
plo8 = img_folder + "pallo8.png"
plo9 = img_folder + "pallo9.png"
plo10 = img_folder + "pallo10.png"

keh1 = img_folder + "keha2.png"
keh_p = img_folder + "keha_p.png"
keh_s = img_folder + "keha_s.png"

pm1 = img_folder + "pum1.png"
pm2 = img_folder + "pum2.png"
pm3 = img_folder + "pum3.png"
pm4 = img_folder + "pum4.png"

maa = img_folder + "maa.jpg"

raj1 = img_folder + "rajahd2.png"
raj3 = img_folder + "rajahd3.png"
raj4 = img_folder + "rajahd4.png"
raj5 = img_folder + "rajahd5.png"

slm1 = img_folder + "salama_t1.png"
slm2 = img_folder + "salama_t2.png"
slm3 = img_folder + "salama_t3.png"
slm4 = img_folder + "salama_t4.png"
slm5 = img_folder + "salama_t5.png"
slm6 = img_folder + "salama_t6.png"
slm7 = img_folder + "salama_t7.png"
slm8 = img_folder + "salama_t8.png"
slm9 = img_folder + "salama_t9.png"

shkp1 = img_folder + "sahkop1.png"
shkp2 = img_folder + "sahkop2.png"
shkp3 = img_folder + "sahkop3.png"
shkp4 = img_folder + "sahkop4.png"
shkp5 = img_folder + "sahkop5.png"
shkp6 = img_folder + "sahkop6.png"
shkp7 = img_folder + "sahkop7.png"
# shkp8 = img_folder + "sahkop8.png"
# shkp9 = img_folder + "sahkop9.png"
shkp10 = img_folder + "sahkop10.png"
shkp11 = img_folder + "sahkop11.png"
shkp12 = img_folder + "sahkop12.png"
shkp13 = img_folder + "sahkop13.png"
shkp14 = img_folder + "sahkop14.png"
shkp15 = img_folder + "sahkop15.png"
shkp16 = img_folder + "sahkop16.png"

slmk1 = img_folder + "salamak3.png"
slmk2 = img_folder + "salamak4.png"
slmk3 = img_folder + "salamak5.png"
slmk4 = img_folder + "salamak6.png"
slmk5 = img_folder + "salamak7.png"
slmk6 = img_folder + "salamak8.png"
slmk7 = img_folder + "salamak9.png"
slmk8 = img_folder + "salamak10.png"
slmk9 = img_folder + "salamak11.png"
slmk10 = img_folder + "salamak12.png"
slmk11 = img_folder + "salamak13.png"
slmk12 = img_folder + "salamak14.png"
slmk13 = img_folder + "salamak15.png"
slmk14 = img_folder + "salamak16.png"
slmk15 = img_folder + "salamak17.png"
slmk16 = img_folder + "salamak18.png"
slmk17 = img_folder + "salamak19.png"
slmk18 = img_folder + "salamak20.png"

sd1 = img_folder + "sade1.png"

prp1 = img_folder + "parap1.png"

pmi1 = img_folder + "pommi1.png"
pmi20 = img_folder + "pommi2_0.png"
pmi21 = img_folder + "pommi2_1.png"
pmi22 = img_folder + "pommi2_2.png"
pmi23 = img_folder + "pommi2_3.png"
pmi24 = img_folder + "pommi2_4.png"

par_r1 = img_folder + "par_raj1.png"
par_r2 = img_folder + "par_raj2.png"
par_r3 = img_folder + "par_raj3.png"
par_r4 = img_folder + "par_raj4.png"
par_r5 = img_folder + "par_raj5.png"
par_r6 = img_folder + "par_raj6.png"

mouk_p1 = img_folder + "moukari_pallo1.png"
mouk_p2 = img_folder + "moukari_pallo2.png"

jlk1 = img_folder + "jalat1.png"
jlk2 = img_folder + "jalat2.png"
jlk3 = img_folder + "jalat3.png"
jlk4 = img_folder + "jalat4.png"

pygame.init()

# ico
icon = pygame.image.load(img_folder + 'grav.ico')
pygame.display.set_icon(icon)

screen = pygame.display.set_mode((xk, yk), 0, 32)

tausta = pygame.image.load(tsta).convert()
nelio = pygame.image.load(nlio).convert()
nelio2 = pygame.image.load(nlio2).convert()
nelio3 = pygame.image.load(nlio3).convert_alpha()
nelio4 = pygame.image.load(nlio4).convert_alpha()
nelio5 = pygame.image.load(nlio5).convert_alpha()
nelio6 = pygame.image.load(nlio6).convert_alpha()
nelio7 = pygame.image.load(nlio7).convert_alpha()
nelio8_0 = pygame.image.load(nlio8_0).convert_alpha()
nelio8_1 = pygame.image.load(nlio8_1).convert_alpha()
nelio8_2 = pygame.image.load(nlio8_2).convert_alpha()
nelio8_3 = pygame.image.load(nlio8_3).convert_alpha()
nelio8_4 = pygame.image.load(nlio8_4).convert_alpha()
nelio8_5 = pygame.image.load(nlio8_5).convert_alpha()
nelio9 = pygame.image.load(nlio9).convert_alpha()
nelio10 = pygame.image.load(nlio10).convert_alpha()
nelio11 = pygame.image.load(nlio11).convert_alpha()

maat = pygame.image.load(maa).convert()
pallo1 = pygame.image.load(plo1).convert_alpha()
pallo2 = pygame.image.load(plo2).convert_alpha()
pallo3 = pygame.image.load(plo3).convert_alpha()
pallo4 = pygame.image.load(plo4).convert_alpha()
pallo5 = pygame.image.load(plo5).convert_alpha()
pallo6 = pygame.image.load(plo6).convert_alpha()
pallo7 = pygame.image.load(plo7).convert_alpha()
pallo8 = pygame.image.load(plo8).convert_alpha()
pallo9 = pygame.image.load(plo9).convert_alpha()
pallo10 = pygame.image.load(plo10).convert_alpha()

keha1 = pygame.image.load(keh1).convert_alpha()

keha_p = pygame.image.load(keh_p).convert_alpha()
keha_s = pygame.image.load(keh_s).convert_alpha()

pum1 = pygame.image.load(pm1).convert_alpha()
pum2 = pygame.image.load(pm2).convert_alpha()
pum3 = pygame.image.load(pm3).convert_alpha()
pum4 = pygame.image.load(pm4).convert_alpha()

rajahd1 = pygame.image.load(raj1).convert_alpha()
rajahd3 = pygame.image.load(raj3).convert_alpha()
rajahd4 = pygame.image.load(raj4).convert_alpha()
rajahd5 = pygame.image.load(raj5).convert_alpha()

salama1 = pygame.image.load(slm1).convert_alpha()
salama2 = pygame.image.load(slm2).convert_alpha()
salama3 = pygame.image.load(slm3).convert_alpha()
salama4 = pygame.image.load(slm4).convert_alpha()
salama5 = pygame.image.load(slm5).convert_alpha()
salama6 = pygame.image.load(slm6).convert_alpha()
salama7 = pygame.image.load(slm7).convert_alpha()
salama8 = pygame.image.load(slm8).convert_alpha()
salama9 = pygame.image.load(slm9).convert_alpha()

sahkop1 = pygame.image.load(shkp1).convert_alpha()
sahkop2 = pygame.image.load(shkp2).convert_alpha()
sahkop3 = pygame.image.load(shkp3).convert_alpha()
sahkop4 = pygame.image.load(shkp4).convert_alpha()
sahkop5 = pygame.image.load(shkp5).convert_alpha()
sahkop6 = pygame.image.load(shkp6).convert_alpha()
sahkop7 = pygame.image.load(shkp7).convert_alpha()
# sahkop8 = pygame.image.load(shkp8).convert_alpha()
# sahkop9 = pygame.image.load(shkp9).convert_alpha()
sahkop10 = pygame.image.load(shkp10).convert_alpha()
sahkop11 = pygame.image.load(shkp11).convert_alpha()
sahkop12 = pygame.image.load(shkp12).convert_alpha()
sahkop13 = pygame.image.load(shkp13).convert_alpha()
sahkop14 = pygame.image.load(shkp14).convert_alpha()
sahkop15 = pygame.image.load(shkp15).convert_alpha()
sahkop16 = pygame.image.load(shkp16).convert_alpha()

# pieni salamankuva ylhaalla

salamak1 = pygame.image.load(slmk1).convert_alpha()
salamak2 = pygame.image.load(slmk2).convert_alpha()
salamak3 = pygame.image.load(slmk3).convert_alpha()
salamak4 = pygame.image.load(slmk4).convert_alpha()
salamak5 = pygame.image.load(slmk5).convert_alpha()
salamak6 = pygame.image.load(slmk6).convert_alpha()
salamak7 = pygame.image.load(slmk7).convert_alpha()
salamak8 = pygame.image.load(slmk8).convert_alpha()
salamak9 = pygame.image.load(slmk9).convert_alpha()
salamak10 = pygame.image.load(slmk10).convert_alpha()
salamak11 = pygame.image.load(slmk11).convert_alpha()
salamak12 = pygame.image.load(slmk12).convert_alpha()
salamak13 = pygame.image.load(slmk13).convert_alpha()
salamak14 = pygame.image.load(slmk14).convert_alpha()
salamak15 = pygame.image.load(slmk15).convert_alpha()
salamak16 = pygame.image.load(slmk16).convert_alpha()
salamak17 = pygame.image.load(slmk17).convert_alpha()
salamak18 = pygame.image.load(slmk18).convert_alpha()

sade1 = pygame.image.load(sd1).convert_alpha()

parap1 = pygame.image.load(prp1).convert_alpha()

pommi1 = pygame.image.load(pmi1).convert_alpha()
pommi20 = pygame.image.load(pmi20).convert_alpha()
pommi21 = pygame.image.load(pmi21).convert_alpha()
pommi22 = pygame.image.load(pmi22).convert_alpha()
pommi23 = pygame.image.load(pmi23).convert_alpha()
pommi24 = pygame.image.load(pmi24).convert_alpha()

par_raj1 = pygame.image.load(par_r1).convert_alpha()
par_raj2 = pygame.image.load(par_r2).convert_alpha()
par_raj3 = pygame.image.load(par_r3).convert_alpha()
par_raj4 = pygame.image.load(par_r4).convert_alpha()
par_raj5 = pygame.image.load(par_r5).convert_alpha()
par_raj6 = pygame.image.load(par_r6).convert_alpha()

moukari_pallo1 = pygame.image.load(mouk_p1).convert_alpha()
moukari_pallo2 = pygame.image.load(mouk_p2).convert_alpha()

jalka1 = pygame.image.load(jlk1).convert_alpha()
jalka2 = pygame.image.load(jlk2).convert_alpha()
jalka3 = pygame.image.load(jlk3).convert_alpha()
jalka4 = pygame.image.load(jlk4).convert_alpha()


class ajan_mittaus():
    def __init__(self):
        self.ekaks = 0
        self.aika = 0
        self.pal_aika = 0

    def ennen_sleep(self):  # toka
        if self.ekaks == 1:
            self.pal_aika = time.clock() - self.aika
        elif self.ekaks == 0:
            self.pal_aika = 0

    def ret_aika(self):
        return self.pal_aika

    def jalkeen_sleep(self):  # eka
        self.ekaks = 1
        self.aika = time.clock()


am = ajan_mittaus()


class itse():
    def ts(self, nro):
        if nro >= 0:
            return nro
        elif nro < 0:
            return -nro
        else:
            pass


i = itse()


class isku_tila():
    def __init__(self):
        self.tila = 0

    def muuta(self, nro1234):
        self.tila = nro1234

    def ret(self):
        return self.tila


isku1 = isku_tila()
isku2 = isku_tila()


class plus_miinus():
    def __init__(self):
        self.yks = -1

    def aja(self):
        self.yks = self.yks * (-1)

    def ret(self):
        return self.yks


pl_mi = plus_miinus()


class health1():
    def __init__(self):

        self.hp = 100

    def muuta_hp(self, muutos):
        self.hp = self.hp + muutos

    def ret_hp(self):
        if self.hp <= 0:
            return 0
        else:
            return self.hp


hp1 = health1()


class health2():
    def __init__(self):

        self.hp = 100

    def muuta_hp(self, muutos):
        self.hp = self.hp + muutos

    def ret_hp(self):
        if self.hp <= 0:
            return 0
        else:
            return self.hp


hp2 = health2()


class erik_isku():
    def __init__(self):
        self.bar = 100
        self.nelio = 1

    def lisaa(self):
        if self.bar < 100:
            self.bar += 0.01
        else:
            pass

    def nelio2(self):
        self.nelio = 2

    def muuta_erik(self, nro):
        self.bar = self.bar + nro

    def ret_bar(self):
        return self.bar


erik1 = erik_isku()

erik2 = erik_isku()
erik2.nelio2()
erik1.ret_bar()


class vaihdanelio():
    def __init__(self):
        self.l = 1

    def lisaa(self):
        self.l += 1
        if self.l == 12:
            self.l = 1

    def lisaa2(self, nro100):
        self.l = nro100
        if self.l > 11:
            self.l = 1

    def nell(self):
        return self.l


vaih = vaihdanelio()
vaih2 = vaihdanelio()
vaih2.lisaa2(8)


class voimat():
    def __init__(self):
        self.ak = 0
        self.ako1 = 0
        self.akv1 = 0
        self.alas = 0

    def akkk(self, ylos):
        self.ak = ylos

    def akkk2(self):
        return self.ak

    def akalas(self, als):
        self.alas = als

    def ret_alas(self):
        return self.alas

    def ako(self, oik):
        self.ako1 = oik

    def akv(self, vas):
        self.akv1 = vas

    def oikea(self):
        return self.ako1

    def vasen(self):
        return self.akv1


objFFF = voimat()
objFFF2 = voimat()


class xyv():
    def __init__(self):
        self.pnro = 1
        self.torm_vx = 0
        self.torm_vy = 0

        self.y = 0
        self.v = 0
        self.m = mm
        self.g = 9.81 * 1.8
        self.b = 35
        if self.pnro == 1:
            self.yy = yk - 40
            self.xx = 20
        elif self.pnro == 2:
            self.yy = y2
            self.xx = x2

        self.vx = 0
        self.x = 0
        self.ax = 0
        self.myy = 0

        self.stop_v = 0  # moukarin jalat pysayttaa

    def pnroo(self):
        self.pnro = 2
        if self.pnro == 2:
            self.yy = y2
            self.xx = x2

    def stop(self):
        self.stop_v = 1

    def anti_stop(self):
        self.stop_v = 0

    def yv1(self):

        t = t1 / 1.4
        if self.pnro == 1:
            self.akk = objFFF.akkk2()
            self.akala = objFFF.ret_alas()
        elif self.pnro == 2:
            self.akk = objFFF2.akkk2()
            self.akala = objFFF2.ret_alas()

        nopeus = math.sqrt((self.v * self.v) + (self.vx * self.vx))

        N = self.v * self.b
        F = (-(self.m * self.akk))
        G = self.m * self.g
        Falas = (self.m * self.akala)

        self.Fkok = (G + F + Falas - N) / self.m + 1.5

        a = self.Fkok
        if self.yy <= 20:
            if a < 0:
                a = 0
        if self.yy >= yk - 40:
            if a > 0:
                a = 0

        self.y = 0

        if self.pnro == 2:
            self.torm_vy = tor.ret_vy2()
        else:
            self.torm_vy = tor.ret_vy1()

        self.vyk = self.v + self.torm_vy + 0.5 * a * t  # keskinopeus ajalle

        if self.stop_v == 1:  # moukarin jalat
            self.vyk = 0
            self.v = 0

        self.v = self.v + self.torm_vy + a * t  # loppunopeus jota kaytet��n taas seuraavissa laskuissa
        self.y = self.y + (self.vyk * t) * 13

        self.torm_vy = 0

        # popmpun esto
        if self.v > -0.3 and self.v < 0.3 and F == 0 and self.yy >= yk - 41:
            self.y = 20

        self.yy = (self.yy) + self.y  # yy paikka
        if self.yy >= yk - 39:
            self.yy = yk - 40
            if self.v > 0:
                self.v = -0.4 * self.v

        if self.yy <= 20:
            if self.v < 0:
                self.v = -0.6 * self.v
            self.yy = 21
        #
        #
        #
        #
        # x suuntainen liike
        if self.yy >= yk - 41 and self.vx <= 0:
            self.myy = 1.2
        elif self.yy >= yk - 41 and self.vx >= 0:
            self.myy = -1.2
        else:
            self.myy = 0

        # print self.vx
        if self.pnro == 1:
            Fv = objFFF.vasen() * self.m
            Fo = objFFF.oikea() * self.m
        elif self.pnro == 2:
            Fv = objFFF2.vasen() * self.m
            Fo = objFFF2.oikea() * self.m

        Nx = self.b * self.vx
        Fmyy = self.myy * self.g * self.m

        Fxx = (Fo - Fv) - Nx

        if Fxx < 0:
            Fx = Fxx + Fmyy
            if Fx > 0:
                Fx = 0
        elif Fxx > 0:
            Fx = Fxx + Fmyy
            if Fx < 0:
                Fx = 0
        elif Fxx == 0:
            Fx = 0

        # print Fxx
        # print Fx
        # print  Fmyy

        self.ax = (Fx) / self.m * 1.5

        if self.x <= 0:
            if self.ax < 0:
                a = 0
        if self.x >= 0:
            if self.ax > 0:
                a = 0

        ## x:n paalike yhtalot
        self.x = 0
        # tormauksen aiheuttama "energia purkaus"
        if self.pnro == 2:
            self.torm_vx = tor.ret_vx2()
        else:
            self.torm_vx = tor.ret_vx1()

        self.vxk = self.vx + 0.5 * self.ax * t
        self.vx = self.vx + self.torm_vx + self.ax * t

        if self.stop_v == 1:  # moukari jalat
            self.vxk = 0
            self.xv = 0

        self.x = self.x + (self.vxk * t) * 13.
        self.torm_vx = 0
        # Pompun esto
        if self.vx < 0.3 and self.vx > -0.3 and F == 0 and Fo == 0 and Fx == 0 and self.yy >= 539:
            self.x = 0

        self.xx = self.xx + self.x
        ##
        if self.xx <= 0:
            self.xx = 1
            if self.vx < 0:
                self.vx = - 0.6 * self.vx
        elif self.xx >= xk - 20:
            self.xx = xk - 21
            if self.vx > 0:
                self.vx = - 0.6 * self.vx

        # print nopeus
        # print self.vx
        # print "vy "+str(self.v)
        # print a

        if self.pnro == 1:
            if hp1.ret_hp() <= 0:
                self.yy == -1000
        if self.pnro == 2:
            if hp2.ret_hp() <= 0:
                self.yy == -1000

        return self.yy

    def rety(self):
        return self.yy

    def retx(self):
        if self.pnro == 1:
            if hp1.ret_hp() <= 0:
                self.xx == -1000
        if self.pnro == 2:
            if hp2.ret_hp() <= 0:
                self.xx == -1000

        return self.xx

    def retvy(self):
        return self.v

    def retvx(self):
        return self.vx


yjav = xyv()

yjav2 = xyv()
yjav2.pnroo()


class pallot():
    def __init__(self):
        self.nro = 0
        self.u = 9999
        self.aaa = 0
        self.dd = 0
        self.pall = 0
        self.tall = 0

        self.kumpi_nelio = 1
        self.vah = 0

    def toka_nelio(self):
        self.kumpi_nelio = 2

    def nro3(self):

        ddlis = t1 * 4
        b = 6.  # voidaan muuttaa energia baarin asetuksia
        if self.nro == 1:
            if self.kumpi_nelio == 1:
                if self.dd < 2 and erik1.ret_bar() >= 0.5 * b:
                    if self.vah == 0 and self.dd >= 1:
                        erik1.muuta_erik(-0.5 * b)
                        self.vah = 1
                    self.dd += ddlis
                elif self.dd >= 2 and self.dd < 3 and erik1.ret_bar() >= (1 / 3.) * b:
                    if self.vah == 1:
                        erik1.muuta_erik(-(1 / 3.) * b)
                        self.vah += 1
                    self.dd += ddlis
                elif self.dd >= 3 and self.dd < 4 and erik1.ret_bar() >= (1 / 4.) * b:
                    if self.vah == 2:
                        erik1.muuta_erik(-(1 / 4.) * b)
                        self.vah += 1
                    self.dd += ddlis
                elif self.dd >= 4 and self.dd < 5 and erik1.ret_bar() >= (1 / 5.) * b:
                    if self.vah == 3:
                        erik1.muuta_erik(-(1 / 5.) * b)
                        self.vah += 1
                    self.dd += ddlis
                elif self.dd >= 5 and self.dd < 6 and erik1.ret_bar() >= (1 / 6.) * b:
                    if self.vah == 4:
                        erik1.muuta_erik(-(1 / 6.) * b)
                        self.vah += 1
                    self.dd += ddlis
                elif self.dd >= 6 and self.dd < 7 and erik1.ret_bar() >= (1 / 7.) * b:
                    if self.vah == 5:
                        erik1.muuta_erik(-(1 / 7.) * b)
                        self.vah += 1
                    self.dd += ddlis
                elif self.dd >= 7 and self.dd < 8 and erik1.ret_bar() >= (1 / 8.) * b:
                    if self.vah == 6:
                        erik1.muuta_erik(-(1 / 8.) * b)
                        self.vah += 1
                    self.dd += ddlis
                elif self.dd >= 8 and self.dd < 9 and erik1.ret_bar() >= (1 / 9.) * b:
                    if self.vah == 7:
                        erik1.muuta_erik(-(1 / 9.) * b)
                        self.vah += 1
                    self.dd += ddlis
                elif self.dd >= 9 and self.dd < 10 and erik1.ret_bar() >= (1 / 10.) * b:
                    if self.vah == 8:
                        erik1.muuta_erik(-(1 / 10.) * b)
                        self.vah += 1
                    self.dd += ddlis
                elif self.dd >= 10 and self.dd < 11 and erik1.ret_bar() >= (1 / 11.) * b:
                    if self.vah == 9:
                        erik1.muuta_erik(-(1 / 11.) * b)
                        self.vah += 1
                    self.dd += ddlis
                else:
                    pass

            elif self.kumpi_nelio == 2:
                if self.dd < 2 and erik2.ret_bar() >= 0.5 * b:
                    if self.vah == 0:
                        erik2.muuta_erik(-0.5 * b)
                        self.vah = 1
                    self.dd += ddlis
                elif self.dd >= 2 and self.dd < 3 and erik2.ret_bar() >= (1 / 3.) * b:
                    if self.vah == 1:
                        erik2.muuta_erik(-(1 / 3.) * b)
                        self.vah += 1
                    self.dd += ddlis
                elif self.dd >= 3 and self.dd < 4 and erik2.ret_bar() >= (1 / 4.) * b:
                    if self.vah == 2:
                        erik2.muuta_erik(-(1 / 4.) * b)
                        self.vah += 1
                    self.dd += ddlis
                elif self.dd >= 4 and self.dd < 5 and erik2.ret_bar() >= (1 / 5.) * b:
                    if self.vah == 3:
                        erik2.muuta_erik(-(1 / 5.) * b)
                        self.vah += 1
                    self.dd += ddlis
                elif self.dd >= 5 and self.dd < 6 and erik2.ret_bar() >= (1 / 6.) * b:
                    if self.vah == 4:
                        erik2.muuta_erik(-(1 / 6.) * b)
                        self.vah += 1
                    self.dd += ddlis
                elif self.dd >= 6 and self.dd < 7 and erik2.ret_bar() >= (1 / 7.) * b:
                    if self.vah == 5:
                        erik2.muuta_erik(-(1 / 7.) * b)
                        self.vah += 1
                    self.dd += ddlis
                elif self.dd >= 7 and self.dd < 8 and erik2.ret_bar() >= (1 / 8.) * b:
                    if self.vah == 6:
                        erik2.muuta_erik(-(1 / 8.) * b)
                        self.vah += 1
                    self.dd += ddlis
                elif self.dd >= 8 and self.dd < 9 and erik2.ret_bar() >= (1 / 9.) * b:
                    if self.vah == 7:
                        erik2.muuta_erik(-(1 / 9.) * b)
                        self.vah += 1
                    self.dd += ddlis
                elif self.dd >= 9 and self.dd < 10 and erik2.ret_bar() >= (1 / 10.) * b:
                    if self.vah == 8:
                        erik2.muuta_erik(-(1 / 10.) * b)
                        self.vah += 1
                    self.dd += ddlis
                elif self.dd >= 10 and self.dd < 11 and erik2.ret_bar() >= (1 / 11.) * b:
                    if self.vah == 9:
                        erik2.muuta_erik(-(1 / 11.) * b)
                        self.vah += 1
                    self.dd += ddlis
                else:
                    pass

    def nollaa_vah(self):
        self.vah = 0

    def nro1(self):
        self.nro = 1

    def nro2(self):  # q on pois
        self.aaa = 0
        self.nro = 0
        self.dd = 0
        self.pall = 0
        self.pall2 = 0
        self.pall3 = 0
        self.pall4 = 0

        self.nro = 0

    def palnro(self):

        if self.dd >= 10:
            pal = 10
        elif self.dd >= 9:
            pal = 9
        elif self.dd >= 8:
            pal = 8
        elif self.dd >= 7:
            pal = 7
        elif self.dd >= 6:
            pal = 6
        elif self.dd >= 5:
            pal = 5
        elif self.dd >= 4:
            pal = 4
        elif self.dd >= 3:
            pal = 3
        elif self.dd >= 2:
            pal = 2
        elif self.dd >= 1:
            pal = 1
        elif self.dd < 1:
            pal = 0

        self.nro111 = pal
        self.pall = pal
        self.pall2 = pal
        self.pall3 = pal
        self.pall4 = pal

        return self.pall

    # pallojen kokojen tallennus
    ##4
    def talletus(self):
        self.tall = self.pall

    def talletus_ret(self):
        return self.tall

    def talletus1_nollaus(self):
        self.tall = 0

    ##2
    def talletus2(self):
        self.tall2 = self.pall

    def talletus2_ret(self):
        return self.tall2

    def talletus2_nollaus(self):
        self.tall2 = 0

    ##3
    def talletus3(self):
        self.tall3 = self.pall

    def talletus3_ret(self):
        return self.tall3

    def talletus3_nollaus(self):
        self.tall3 = 0

    ##4
    def talletus4(self):
        self.tall4 = self.pall

    def talletus4_ret(self):
        return self.tall4

    def talletus4_nollaus(self):
        self.tall4 = 0

    def palyk(self):
        if self.nro111 >= 1 and self.nro111 <= 10:
            self.u = ((20 - self.nro111 * 4) / 2)
            return self.u
        else:
            return 0


objpallot = pallot()

objpallot2 = pallot()
objpallot2.toka_nelio()

objpallot.nollaa_vah()


class pallon_paikka():  # jos alussa ei ole liikunut voi tulla ongelmia

    def __init__(self):

        self.kumpi_nelio = 1

        self.koko = 0
        self.vxp = 0
        self.vyp = 0

        self.alku_y = -100
        self.alku_x = -100
        self.pnro = 1

        self.matkalla = 0

    def mones_pallo(self, nro2):
        self.pnro = nro2

    def toka_nelio(self):
        self.kumpi_nelio = 2

    def alku1(self):
        if self.kumpi_nelio == 1:
            if self.pnro == 1:
                self.koko = objpallot.talletus_ret()
            elif self.pnro == 2:
                self.koko = objpallot.talletus2_ret()
            elif self.pnro == 3:
                self.koko = objpallot.talletus3_ret()
            elif self.pnro == 4:
                self.koko = objpallot.talletus4_ret()
        elif self.kumpi_nelio == 2:
            if self.pnro == 1:
                self.koko = objpallot2.talletus_ret()
            elif self.pnro == 2:
                self.koko = objpallot2.talletus2_ret()
            elif self.pnro == 3:
                self.koko = objpallot2.talletus3_ret()
            elif self.pnro == 4:
                self.koko = objpallot2.talletus4_ret()

    def totuus1(self):
        if self.koko == 0:
            return 0
        else:
            return 1

    def laskuja1(self):
        if self.koko == 0:
            pass
        else:
            self.matkalla = 1
            if self.kumpi_nelio == 1:
                vyn = yjav.retvy()
                vxn = yjav.retvx()
                objpallot.nollaa_vah()
            elif self.kumpi_nelio == 2:
                vyn = yjav2.retvy()
                vxn = yjav2.retvx()
                objpallot2.nollaa_vah()

            if self.pnro == 1:
                xvau = 150
                yvau = 0
            if self.pnro == 2:
                xvau = 0
                yvau = 150
            if self.pnro == 3:
                xvau = -150
                yvau = 0
            if self.pnro == 4:
                xvau = 0
                yvau = -150

            self.vxp = xvau + vxn
            self.vyp = yvau + vyn

            if self.kumpi_nelio == 1:
                self.alku_y = yjav.rety()
                self.alku_x = yjav.retx()
            elif self.kumpi_nelio == 2:
                self.alku_y = yjav2.rety()
                self.alku_x = yjav2.retx()

    def nollaus_alku(self):
        if self.kumpi_nelio == 1:
            if self.pnro == 1:
                objpallot.talletus1_nollaus()
            elif self.pnro == 2:
                objpallot.talletus2_nollaus()
            elif self.pnro == 3:
                objpallot.talletus3_nollaus()
            elif self.pnro == 4:
                objpallot.talletus4_nollaus()
        elif self.kumpi_nelio == 2:
            if self.pnro == 1:
                objpallot2.talletus1_nollaus()
            elif self.pnro == 2:
                objpallot2.talletus2_nollaus()
            elif self.pnro == 3:
                objpallot2.talletus3_nollaus()
            elif self.pnro == 4:
                objpallot2.talletus4_nollaus()

    def paikka1(self):

        t = t1 / 1.35
        self.alku_x = self.alku_x + (self.vxp * t) * 13
        self.alku_y = self.alku_y + (self.vyp * t) * 13

        if self.alku_x >= (xk):
            self.alku_x = -100
            self.alku_y = -100
            self.nollaus_alku()

            self.koko = 0
            self.matkalla = 0
        elif self.alku_y >= (yk):
            self.alku_x = -100
            self.alku_y = -100
            self.nollaus_alku()

            self.koko = 0
            self.matkalla = 0
        elif self.alku_y <= 20:
            self.alku_x = -100
            self.alku_y = -100
            self.nollaus_alku()

            self.koko = 0
            self.matkalla = 0
        elif self.alku_x <= 0:
            self.alku_x = -100
            self.alku_y = -100
            self.nollaus_alku()

            self.koko = 0
            self.matkalla = 0
        else:
            pass

    def pallon_nollaus_ulkoota(self):
        self.alku_x = -100
        self.alku_y = -100
        self.nollaus_alku()

        self.koko = 0
        self.matkalla = 0

    def ret_pallo1_x(self):
        if self.alku_x == -100:
            return 0
        else:
            return self.alku_x

    def ret_pallo1_y(self):
        return self.alku_y

    def ret_matkalla(self):
        return self.matkalla


pp = pallon_paikka()
pp2 = pallon_paikka()
pp3 = pallon_paikka()
pp4 = pallon_paikka()

p2p = pallon_paikka()
p2p2 = pallon_paikka()
p2p3 = pallon_paikka()
p2p4 = pallon_paikka()

p2p.toka_nelio()
p2p2.toka_nelio()
p2p3.toka_nelio()
p2p4.toka_nelio()


class tormays():  # NELIODEN suojat tormaa
    def __init__(self):
        self.aika1 = 2

        self.vali_x = 0
        self.vali_y = 0

        self.x1 = 0
        self.y1 = 0
        self.x2 = 0
        self.y2 = 0

        self.vauhti_x1 = 0
        self.vauhti_y1 = 0
        self.vauhti_x2 = 0
        self.vauhti_y2 = 0
        self.hae_paikka()

    def aika(self):
        self.vauhti_x1 = 0
        self.vauhti_y1 = 0
        self.vauhti_x2 = 0
        self.vauhti_y2 = 0
        if self.aika1 <= 2:
            self.aika1 += t1 / 13
        else:
            pass

    def hae_paikka(self):
        self.x1 = yjav.retx()
        self.y1 = yjav.rety()
        self.x2 = yjav2.retx()
        self.y2 = yjav2.rety()
        if self.aika1 > 0.02:
            self.palikka()

    def palikka(self):
        kx1 = self.x1 + 10
        ky1 = self.y1 + 10
        kx2 = self.x2 + 10
        ky2 = self.y2 + 10

        distan = math.sqrt((kx1 - kx2) ** 2 + (ky1 - ky2) ** 2)

        if distan <= 30.5 and self.aika1 > 0.02 and hp1.ret_hp() > 0 and hp2.ret_hp() > 0:
            hp1.muuta_hp(-15)
            hp2.muuta_hp(-15)
            if self.x1 >= self.x2:
                x1ker = 1
                x2ker = -1
            elif self.x1 < self.x2:
                x1ker = -1
                x2ker = 1

            if self.y1 >= self.y2:
                y1ker = 1
                y2ker = -1
            elif self.y1 < self.y2:
                y1ker = -1
                y2ker = 1

            self.vauhti_x1 = x1ker * math.sqrt((kx1 - kx2) ** 2) * 4
            self.vauhti_y1 = y1ker * math.sqrt((ky1 - ky2) ** 2) * 4
            self.vauhti_x2 = x2ker * math.sqrt((kx1 - kx2) ** 2) * 4
            self.vauhti_y2 = y2ker * math.sqrt((ky1 - ky2) ** 2) * 4
            # rajahdys kuvan paikka

            self.vali_x = 0
            self.vali_y = 0

            self.vali_x = (kx1 + kx2) / 2
            self.vali_y = (ky1 + ky2) / 2

            self.aika1 = 0

    def ret_vx1(self):
        return self.vauhti_x1
        self.vauhti_x1 = 0

    def ret_vy1(self):
        return self.vauhti_y1
        self.vauhti_y1 = 0

    def ret_vx2(self):
        return self.vauhti_x2
        self.vauhti_x2 = 0

    def ret_vy2(self):
        return self.vauhti_y2
        self.vauhti_y2 = 0

    def ret_valix(self):
        return self.vali_x

    def ret_valiy(self):
        return self.vali_y

    def ret_aika1(self):
        return self.aika1


class liikuva_pallo_tormaa_nelioon1():
    def __init__(self):
        self.matka1 = 0
        self.matka2 = 0
        self.matka3 = 0
        self.matka4 = 0
        self.raj_vakio = 0
        self.raj_x = 0
        self.raj_y = 0

    def hae_ja_laske(self):
        # 1. nelion paikat
        self.x1 = yjav.retx() + 10
        self.y1 = yjav.rety() + 10

        # pallojen koot 1-10(pit�si olla samat jos kaikki on jaljella)

        pallon_koko1 = objpallot2.talletus_ret() * 2
        pallon_koko2 = objpallot2.talletus2_ret() * 2
        pallon_koko3 = objpallot2.talletus3_ret() * 2
        pallon_koko4 = objpallot2.talletus4_ret() * 2

        # "vastustajan" pallojen keskipisteiden paikat 2 nelion pallot
        ypallol2 = objpallot2.palyk()

        p1x = p2p.ret_pallo1_x() + 35 + pallon_koko1
        p2x = p2p2.ret_pallo1_x() + ypallol2 + pallon_koko2
        p3x = p2p3.ret_pallo1_x() - 15 - 2 * pallon_koko3 + pallon_koko3
        p4x = p2p4.ret_pallo1_x() + ypallol2 + pallon_koko4

        p1y = p2p.ret_pallo1_y() + ypallol2 + pallon_koko1
        p2y = p2p2.ret_pallo1_y() + 35 + pallon_koko2
        p3y = p2p3.ret_pallo1_y() + ypallol2 + pallon_koko3
        p4y = p2p4.ret_pallo1_y() - 15 - 2 * pallon_koko4 + pallon_koko4

        self.matka1 = math.sqrt((self.x1 - p1x) ** 2 + (self.y1 - p1y) ** 2)
        self.matka2 = math.sqrt((self.x1 - p2x) ** 2 + (self.y1 - p2y) ** 2)
        self.matka3 = math.sqrt((self.x1 - p3x) ** 2 + (self.y1 - p3y) ** 2)
        self.matka4 = math.sqrt((self.x1 - p4x) ** 2 + (self.y1 - p4y) ** 2)

        kriit_dist1 = 15 + pallon_koko1
        kriit_dist2 = 15 + pallon_koko2
        kriit_dist3 = 15 + pallon_koko3
        kriit_dist4 = 15 + pallon_koko4

        if self.matka1 <= kriit_dist1:
            hp1.muuta_hp(math.sqrt(pallon_koko1) * -5)
            self.raj_x = self.x1 + math.sqrt((self.x1 - p1x) ** 2) - pallon_koko1
            self.raj_y = self.y1 + math.sqrt((self.y1 - p1y) ** 2) - pallon_koko1
            self.raj_paikka()
            p2p.pallon_nollaus_ulkoota()

        if self.matka2 <= kriit_dist2:
            hp1.muuta_hp(math.sqrt(pallon_koko2) * -5)
            self.raj_x = self.x1 + math.sqrt((self.x1 - p2x) ** 2) - pallon_koko2
            self.raj_y = self.y1 + math.sqrt((self.y1 - p2y) ** 2) - pallon_koko2
            self.raj_paikka()

            p2p2.pallon_nollaus_ulkoota()

        if self.matka3 <= kriit_dist3:
            hp1.muuta_hp(math.sqrt(pallon_koko3) * -5)
            self.raj_x = self.x1 + math.sqrt((self.x1 - p3x) ** 2) - pallon_koko3
            self.raj_y = self.y1 + math.sqrt((self.y1 - p3y) ** 2) - pallon_koko3
            self.raj_paikka()

            p2p3.pallon_nollaus_ulkoota()

        if self.matka4 <= kriit_dist4:
            hp1.muuta_hp(math.sqrt(pallon_koko4) * -5)
            self.raj_x = self.x1 + math.sqrt((self.x1 - p4x) ** 2) - pallon_koko4
            self.raj_y = self.y1 + math.sqrt((self.y1 - p4y) ** 2) - pallon_koko4
            self.raj_paikka()

            p2p4.pallon_nollaus_ulkoota()


            # print self.raj_y

    def raj_paikka(self):
        self.raj_vakio = 1

    def nollaa_raj_vakio(self):
        self.raj_vakio = 0

    def aika1001(self):
        return self.raj_vakio

    def ret_raj_x(self):
        return self.raj_x

    def ret_raj_y(self):
        return self.raj_y


lp1 = liikuva_pallo_tormaa_nelioon1()


####

class liikuva_pallo_tormaa_nelioon2():
    def __init__(self):
        self.matka1 = 0
        self.matka2 = 0
        self.matka3 = 0
        self.matka4 = 0

        self.raj_vakio = 0

    def hae_ja_laske(self):
        # 2. nelion paikat
        self.x1 = yjav2.retx() + 10
        self.y1 = yjav2.rety() + 10

        # palljen koot 1-10(pit�si olla samat jos kaikki on jaljella)
        # print objpallot2.talletus3_ret()

        pallon_koko1 = objpallot.talletus_ret() * 2
        pallon_koko2 = objpallot.talletus2_ret() * 2
        pallon_koko3 = objpallot.talletus3_ret() * 2
        pallon_koko4 = objpallot.talletus4_ret() * 2

        # "vastustajan" pallojen keskipisteiden paikat 1 nelion pallot
        ypallol2 = objpallot.palyk()

        p1x = pp.ret_pallo1_x() + 35 + pallon_koko1
        p2x = pp2.ret_pallo1_x() + ypallol2 + pallon_koko2
        p3x = pp3.ret_pallo1_x() - 15 - 2 * pallon_koko3 + pallon_koko3
        p4x = pp4.ret_pallo1_x() + ypallol2 + pallon_koko4

        p1y = pp.ret_pallo1_y() + ypallol2 + pallon_koko1
        p2y = pp2.ret_pallo1_y() + 35 + pallon_koko2
        p3y = pp3.ret_pallo1_y() + ypallol2 + pallon_koko3
        p4y = pp4.ret_pallo1_y() - 15 - 2 * pallon_koko4 + pallon_koko4

        self.matka1 = math.sqrt((self.x1 - p1x) ** 2 + (self.y1 - p1y) ** 2)
        self.matka2 = math.sqrt((self.x1 - p2x) ** 2 + (self.y1 - p2y) ** 2)
        self.matka3 = math.sqrt((self.x1 - p3x) ** 2 + (self.y1 - p3y) ** 2)
        self.matka4 = math.sqrt((self.x1 - p4x) ** 2 + (self.y1 - p4y) ** 2)

        kriit_dist1 = 15 + pallon_koko1
        kriit_dist2 = 15 + pallon_koko2
        kriit_dist3 = 15 + pallon_koko3
        kriit_dist4 = 15 + pallon_koko4

        if self.matka1 <= kriit_dist1:
            hp2.muuta_hp(math.sqrt(pallon_koko1) * -5)

            self.raj_x = self.x1 + math.sqrt((self.x1 - p1x) ** 2) - pallon_koko1
            self.raj_y = self.y1 + math.sqrt((self.y1 - p1y) ** 2) - pallon_koko1
            self.raj_paikka()
            pp.pallon_nollaus_ulkoota()

        if self.matka2 <= kriit_dist2:
            hp2.muuta_hp(math.sqrt(pallon_koko2) * -5)

            self.raj_x = self.x1 + math.sqrt((self.x1 - p2x) ** 2) - pallon_koko2
            self.raj_y = self.y1 + math.sqrt((self.y1 - p2y) ** 2) - pallon_koko2
            self.raj_paikka()
            pp2.pallon_nollaus_ulkoota()

        if self.matka3 <= kriit_dist3:
            hp2.muuta_hp(math.sqrt(pallon_koko3) * -5)

            self.raj_x = self.x1 + math.sqrt((self.x1 - p3x) ** 2) - pallon_koko3
            self.raj_y = self.y1 + math.sqrt((self.y1 - p3y) ** 2) - pallon_koko3
            self.raj_paikka()
            pp3.pallon_nollaus_ulkoota()

        if self.matka4 <= kriit_dist4:
            hp2.muuta_hp(math.sqrt(pallon_koko4) * -5)

            self.raj_x = self.x1 + math.sqrt((self.x1 - p4x) ** 2) - pallon_koko4
            self.raj_y = self.y1 + math.sqrt((self.y1 - p4y) ** 2) - pallon_koko4
            self.raj_paikka()
            pp4.pallon_nollaus_ulkoota()

    def raj_paikka(self):
        self.raj_vakio = 1

    def nollaa_raj_vakio(self):
        self.raj_vakio = 0

    def aika1001(self):
        return self.raj_vakio

    def ret_raj_x(self):  # tehty turhaksi nyt kaytan pelkastaan nelion koordinaatteja
        return self.raj_x

    def ret_raj_y(self):
        return self.raj_y


lp2 = liikuva_pallo_tormaa_nelioon2()

tor = tormays()


class csalama():
    def __init__(self):
        self.onoff = 0
        self.aika_m = 0
        self.nnro = 1

        self.x = -100
        self.y = -100
        self.vx = 0
        self.vy = 0

        self.yt = 0
        self.yli = 0
        self.kerra = 0
        self.matkalla = 0

        self.blitti = 0

        self.iskuv = 0

        self.paljo_energiaa = 0

    def nelio(self):
        self.nnro = 2

    def salama_on(self):
        if self.nnro == 1 and erik1.ret_bar() > 0.75:
            self.onoff = 1
        elif self.nnro == 2 and erik2.ret_bar() > 0.75:
            self.onoff = 1
        else:
            pass

    def salama_off(self):
        self.onoff = 0

    def ret_onoff(self):
        return self.onoff

    def aika(self):
        t = t1 * 10 / 1.35

        if self.onoff == 1:

            self.bli_sa()
            self.matkalla += (t1 * 100 / 1.35)

            if self.yli == 0:

                if self.nnro == 1:

                    if self.kerra == 0:
                        self.x = yjav.retx() - 11
                        self.y = yjav.rety() + 42 + 42 + 42
                        self.kerra = 1

                    self.vx = yjav.retvx()
                    self.vy = yjav.retvy()

                    self.yt = yjav.rety()


                elif self.nnro == 2:
                    if self.kerra == 0:
                        self.x = yjav2.retx() - 11
                        self.y = yjav2.rety() + 42 + 42 + 42
                        self.kerra = 1

                    self.vx = yjav2.retvx()
                    self.vy = yjav2.retvy()

                    self.yt = yjav2.rety()

                self.vxkok = self.vx / 10
                self.vykok = self.vy / 10 - 140

                self.x = self.x + self.vxkok
                self.y = self.y + self.vykok

                if self.y <= self.yt + 42:  # mieti viell�
                    self.yli = 1


            elif self.yli == 1:

                self.x = self.x + self.vxkok * t
                self.y = self.y + self.vykok * t
                if self.matkalla >= 60 and self.paljo_energiaa < 15:
                    if self.nnro == 1:  # ERIK BARI
                        erik1.muuta_erik(-1.5)
                    elif self.nnro == 2:
                        erik2.muuta_erik(-1.5)
                    self.paljo_energiaa += 1

                    self.kerra = 0
                    self.yli = 0

                    self.x = -100
                    self.y = -100
                    self.vx = 0
                    self.vy = 0
                    self.yt = 0

                    self.matkalla = 0


        else:

            if self.onoff == 1:
                self.aika()
            else:

                self.kerra = 0
                self.matkalla = 0

                # self.aika_m = 0
                self.x = -100
                self.y = -100
                self.vx = 0
                self.vy = 0
                self.yli = 0

                self.yt = 0

    def bli_sa(self):
        if self.nnro == 1:
            tot = hp1.ret_hp()
        elif self.nnro == 2:
            tot = hp2.ret_hp()

        if tot > 0:
            self.blitti += (t1 * 100 / 1.35)

            if self.blitti < 2:
                sahkopX = sahkop1

            elif self.blitti < 4:
                sahkopX = sahkop1

            elif self.blitti < 6:
                sahkopX = sahkop2

            elif self.blitti < 8:
                sahkopX = sahkop3

            elif self.blitti < 10:
                sahkopX = sahkop4

            elif self.blitti < 12:
                sahkopX = sahkop5

            elif self.blitti < 14:
                sahkopX = sahkop6

            elif self.blitti < 16:
                sahkopX = sahkop7

                # elif self.blitti <18:
                # sahkopX = sahkop8

                # elif self.blitti <20:
                # sahkopX = sahkop9

            elif self.blitti < 18:
                sahkopX = sahkop10

            elif self.blitti < 20:
                sahkopX = sahkop11

            elif self.blitti < 22:
                sahkopX = sahkop12

            elif self.blitti < 24:
                sahkopX = sahkop13

            elif self.blitti < 26:
                sahkopX = sahkop14

            elif self.blitti < 28:
                sahkopX = sahkop15

            elif self.blitti <= 31:
                sahkopX = sahkop16

            if self.blitti >= 30:
                self.blitti = 0

            screen.blit(sahkopX, (self.x, self.y))

    def energia(self):
        return self.paljo_energiaa

    def iske(self):  # tasta tehdaan sittenkin isku
        if self.nnro == 1:
            tot2 = hp2.ret_hp()
        elif self.nnro == 2:
            tot2 = hp1.ret_hp()

        if self.paljo_energiaa > 0 and tot2 > 0:

            if self.nnro == 2:

                self.x = yjav.retx() + 10
                self.y = yjav.rety() - (yk - 20) + 10

            elif self.nnro == 1:

                self.x = yjav2.retx() + 10
                self.y = yjav2.rety() - (yk - 20) + 10

            if self.aika_m < 3:

                screen.blit(salama1, (self.x, self.y))
                self.iskuv += 1

                self.aika_m += 1

            elif self.aika_m <= 6:  #
                screen.blit(salama7, (self.x - 75, self.y))
                self.aika_m += 1
                self.iskuv += 1

            elif self.aika_m <= 9:
                screen.blit(salama4, (self.x - 150, self.y))
                self.aika_m += 1
                self.iskuv += 1
            elif self.aika_m <= 12:  #
                screen.blit(salama7, (self.x - 75, self.y))
                self.aika_m += 1
                self.iskuv += 1

            elif self.aika_m <= 15:
                screen.blit(salama2, (self.x, self.y))
                self.aika_m += 1
                self.iskuv += 1

            elif self.aika_m <= 18:  #
                screen.blit(salama8, (self.x - 75, self.y))
                self.aika_m += 1
                self.iskuv += 1

            elif self.aika_m <= 21:
                screen.blit(salama5, (self.x - 150, self.y))
                self.aika_m += 1
                self.iskuv += 1

            elif self.aika_m <= 24:  #
                screen.blit(salama8, (self.x - 75, self.y))
                self.aika_m += 1
                self.iskuv += 1

            elif self.aika_m <= 27:
                screen.blit(salama3, (self.x, self.y))
                self.aika_m += 1
                self.iskuv += 1

            elif self.aika_m <= 30:  #
                screen.blit(salama9, (self.x - 75, self.y))
                self.aika_m += 1
                self.iskuv += 1

            elif self.aika_m <= 33:
                screen.blit(salama6, (self.x - 150, self.y))
                self.aika_m += 1
                self.iskuv += 1

            elif self.aika_m <= 36:  #
                screen.blit(salama9, (self.x - 75, self.y))
                self.aika_m += 1
                self.iskuv += 1

            else:
                screen.blit(salama1, (self.x, self.y))
                self.aika_m = 0

            if self.iskuv >= 10:
                self.paljo_energiaa -= 1
                if self.nnro == 1:
                    hp2.muuta_hp(-3.0)
                elif self.nnro == 2:
                    hp1.muuta_hp(-3.0)
                self.iskuv = 0
        else:
            self.aika_m = 0

    def salamak(self):  # peni salama ikoni
        if self.paljo_energiaa < 1:
            pass
        else:
            if self.nnro == 1:
                koorx = 200
                koory = 19 - self.paljo_energiaa - 3
            elif self.nnro == 2:
                koorx = xk - 20
                koory = 19 - self.paljo_energiaa - 3

            if self.paljo_energiaa == 1:
                screen.blit(salamak2, (koorx, koory + 2))
            elif self.paljo_energiaa == 2:
                screen.blit(salamak4, (koorx, koory + 1))
            elif self.paljo_energiaa == 3:
                screen.blit(salamak6, (koorx, koory))
            elif self.paljo_energiaa == 4:
                screen.blit(salamak7, (koorx, koory))
            elif self.paljo_energiaa == 5:
                screen.blit(salamak8, (koorx, koory))
            elif self.paljo_energiaa == 6:
                screen.blit(salamak9, (koorx, koory))
            elif self.paljo_energiaa == 7:
                screen.blit(salamak10, (koorx, koory))
            elif self.paljo_energiaa == 8:
                screen.blit(salamak11, (koorx, koory))
            elif self.paljo_energiaa == 9:
                screen.blit(salamak12, (koorx, koory))
            elif self.paljo_energiaa == 10:
                screen.blit(salamak13, (koorx, koory))
            elif self.paljo_energiaa == 11:
                screen.blit(salamak14, (koorx, koory))
            elif self.paljo_energiaa == 12:
                screen.blit(salamak15, (koorx, koory))
            elif self.paljo_energiaa == 13:
                screen.blit(salamak16, (koorx, koory))
            elif self.paljo_energiaa == 14:
                screen.blit(salamak17, (koorx, koory))
            elif self.paljo_energiaa == 15:
                screen.blit(salamak18, (koorx, koory))


osalama1 = csalama()
osalama2 = csalama()
osalama2.nelio()
sal1 = 0
sal2 = 0


class par_ammus():
    def __init__(self):
        self.y = 20
        self.x = xk / 2

        self.vy = 0
        self.vx = 0

        self.x0 = xk / 2
        self.x6 = xk / 2
        self.x12 = xk / 2
        self.x18 = xk / 2
        self.x24 = xk / 2
        self.x30 = xk / 2
        self.x36 = xk / 2
        self.x42 = xk / 2
        self.x48 = xk / 2

        self.x6n = xk / 2
        self.x12n = xk / 2
        self.x18n = xk / 2
        self.x24n = xk / 2
        self.x30n = xk / 2
        self.x36n = xk / 2
        self.x42n = xk / 2
        self.x48n = xk / 2

        self.nelio = 1

        self.ekat_koord = 1
        self.lahto = 0

        self.kerroin = 0

        self.aika = 0

    def nelio2(self):
        self.nelio = 2

    def nappi_alas(self):  # run kerran kun nappi painetaa
        if self.lahto == 0:
            if self.nelio == 1:
                self.nx = yjav.retx()
                self.ny = yjav.rety()

            elif self.nelio == 2:
                self.nx = yjav2.retx()
                self.ny = yjav2.rety()

            self.xp = self.nx
            self.yp = self.ny

            self.kerroin = ((math.floor(time.time() * 7.588)) % 84) * pl_mi.ret()

            self.lahto = 1
        else:
            pass

    def ammus_laskut(self):  # run koko ajan
        if self.lahto == 0:
            pass
        elif self.lahto == 1:
            tavoite_x = xk / 2 + self.kerroin
            tavoite_y = 20

            if math.sqrt((self.nx - tavoite_x) ** 2 + (self.ny - tavoite_y) ** 2) <= 8:
                if self.nelio == 1:
                    isku1.muuta(0)
                elif self.nelio == 2:
                    isku2.muuta(0)
                self.lahto = 2

            else:
                b = t1 * 100 * 11 / (math.sqrt((self.xp - tavoite_x) ** 2 + (self.yp - tavoite_y) ** 2))
                self.xp = self.xp - (self.xp - tavoite_x) * b
                self.yp = self.yp - (self.yp - tavoite_y) * b
                et = (math.sqrt((self.xp - tavoite_x) ** 2 + (self.yp - tavoite_y) ** 2))
                if et < 50:
                    screen.blit(pommi20, (self.xp - 40, self.yp))
                elif et < 200:
                    screen.blit(pommi21, (self.xp - 40, self.yp))
                elif et < 350:
                    screen.blit(pommi22, (self.xp - 40, self.yp))
                elif et < 500:
                    screen.blit(pommi23, (self.xp - 40, self.yp))
                elif et >= 500:
                    screen.blit(pommi24, (self.xp - 40, self.yp))

                # print b
                # print self.xp
                # print self.yp
                if math.sqrt((self.xp - tavoite_x) ** 2 + (self.yp - tavoite_y) ** 2) <= 8:
                    if self.nelio == 1:
                        isku1.muuta(0)
                    elif self.nelio == 2:
                        isku2.muuta(0)
                    self.lahto = 2

    def ret_lahto(self):
        return self.lahto

    def aloita(self):
        self.y = 20
        self.x = xk / 2 + self.kerroin

    def lentorata(self):
        if self.lahto == 2:
            t = t1 * 6
            ay = 10.

            self.vyk = self.vy + 0.5 * ay * t
            self.vy = self.vy + ay * t
            self.y = self.y + self.vyk * t

            self.x0 = self.x0 + 0 * t + self.kerroin
            self.x6 = self.x6 + 6 * t + self.kerroin
            self.x12 = self.x12 + 12 * t + self.kerroin
            self.x18 = self.x18 + 18 * t + self.kerroin
            self.x24 = self.x24 + 24 * t + self.kerroin
            self.x30 = self.x30 + 30 * t + self.kerroin
            self.x36 = self.x36 + 36 * t + self.kerroin
            self.x42 = self.x42 + 42 * t + self.kerroin
            self.x48 = self.x48 + 48 * t + self.kerroin

            self.x6n = self.x6n - 6 * t + self.kerroin
            self.x12n = self.x12n - 12 * t + self.kerroin
            self.x18n = self.x18n - 18 * t + self.kerroin
            self.x24n = self.x24n - 24 * t + self.kerroin
            self.x30n = self.x30n - 30 * t + self.kerroin
            self.x36n = self.x36n - 36 * t + self.kerroin
            self.x42n = self.x42n - 42 * t + self.kerroin
            self.x48n = self.x48n - 48 * t + self.kerroin

            self.kerroin = 0

            if self.nelio == 2:  # numerot ristiin, koska haetaan osumaa...
                nelio_x = yjav.retx()
                nelio_y = yjav.rety()
            elif self.nelio == 1:
                nelio_x = yjav2.retx()
                nelio_y = yjav2.rety()

            if i.ts(nelio_y + 10 - self.y - 2) > 17.5:
                pass
            else:  # rajahdys
                if i.ts(nelio_x + 10 - (self.x0 + 2)) < 17.5 or i.ts(nelio_x + 10 - (self.x6 + 2)) < 17.5 or i.ts(
                                        nelio_x + 10 - (self.x12 + 2)) < 17.5 or i.ts(
                                        nelio_x + 10 - (self.x18 + 2)) < 17.5 or i.ts(
                                        nelio_x + 10 - (self.x24 + 2)) < 17.5 or i.ts(
                                        nelio_x + 10 - (self.x30 + 2)) < 17.5 or i.ts(
                                        nelio_x + 10 - (self.x36 + 2)) < 17.5 or i.ts(
                                        nelio_x + 10 - (self.x42 + 2)) < 17.5 or i.ts(
                                        nelio_x + 10 - (self.x48 + 2)) < 17.5 or i.ts(
                                        nelio_x + 10 - (self.x6n + 2)) < 17.5 or i.ts(
                                        nelio_x + 10 - (self.x12n + 2)) < 17.5 or i.ts(
                                        nelio_x + 10 - (self.x18n + 2)) < 17.5 or i.ts(
                                        nelio_x + 10 - (self.x24n + 2)) < 17.5 or i.ts(
                                        nelio_x + 10 - (self.x30n + 2)) < 17.5 or i.ts(
                                        nelio_x + 10 - (self.x36n + 2)) < 17.5 or i.ts(
                                        nelio_x + 10 - (self.x42n + 2)) < 17.5 or i.ts(
                                        nelio_x + 10 - (self.x48n + 2)) < 17.5:
                    self.vy = 0
                    self.vx = 0

                    self.x0 = xk / 2
                    self.x6 = xk / 2
                    self.x12 = xk / 2
                    self.x18 = xk / 2
                    self.x24 = xk / 2
                    self.x30 = xk / 2
                    self.x36 = xk / 2
                    self.x42 = xk / 2
                    self.x48 = xk / 2

                    self.x6n = xk / 2
                    self.x12n = xk / 2
                    self.x18n = xk / 2
                    self.x24n = xk / 2
                    self.x30n = xk / 2
                    self.x36n = xk / 2
                    self.x42n = xk / 2
                    self.x48n = xk / 2
                    self.y = 0
                    if self.nelio == 1:
                        hp2.muuta_hp(-40)
                    elif self.nelio == 2:
                        hp1.muuta_hp(-40)
                    self.lahto = 4

            if self.y >= 560:
                self.vy = 0
                self.vx = 0

                self.x0 = xk / 2
                self.x6 = xk / 2
                self.x12 = xk / 2
                self.x18 = xk / 2
                self.x24 = xk / 2
                self.x30 = xk / 2
                self.x36 = xk / 2
                self.x42 = xk / 2
                self.x48 = xk / 2

                self.x6n = xk / 2
                self.x12n = xk / 2
                self.x18n = xk / 2
                self.x24n = xk / 2
                self.x30n = xk / 2
                self.x36n = xk / 2
                self.x42n = xk / 2
                self.x48n = xk / 2
                self.y = 20
                self.lahto = 0  # pommin pamahtaa maahan

        if self.lahto == 2 or self.lahto == 4:
            self.draw_()


        else:
            pass

    def draw_(self):
        if self.lahto == 4:
            self.aika += t1 * 100 / 1.4
            if self.nelio == 1:
                xx = yjav2.retx() + 10
                yy = yjav2.rety() + 10
            elif self.nelio == 2:
                xx = yjav.retx() + 10
                yy = yjav.rety() + 10

            if self.aika < 4:
                screen.blit(par_raj5, (xx - 820, yy - 50))
            elif self.aika < 7:
                screen.blit(par_raj4, (xx - 820, yy - 50))
            elif self.aika < 10:
                screen.blit(par_raj3, (xx - 820, yy - 50))
            elif self.aika < 13:
                screen.blit(par_raj2, (xx - 820, yy - 50))
            elif self.aika < 16:
                screen.blit(par_raj1, (xx - 820, yy - 50))
            elif self.aika < 18:
                screen.blit(par_raj6, (xx - 820, yy - 50))
            else:
                self.aika = 0
                self.lahto = 0

        if self.lahto == 2:
            # pygame.draw.rect(tausta,(255,255,255),Rect((self.x,self.y),(4,4)))
            screen.blit(sade1, (self.x0 - 2, self.y + 2))
            screen.blit(sade1, (self.x6 - 2, self.y + 2))
            screen.blit(sade1, (self.x12 - 2, self.y + 2))
            screen.blit(sade1, (self.x18 - 2, self.y + 2))
            screen.blit(sade1, (self.x24 - 2, self.y + 2))
            screen.blit(sade1, (self.x30 - 2, self.y + 2))
            screen.blit(sade1, (self.x36 - 2, self.y + 2))
            screen.blit(sade1, (self.x42 - 2, self.y + 2))
            screen.blit(sade1, (self.x48 - 2, self.y + 2))
            screen.blit(sade1, (self.x6n - 2, self.y + 2))
            screen.blit(sade1, (self.x12n - 2, self.y + 2))
            screen.blit(sade1, (self.x18n - 2, self.y + 2))
            screen.blit(sade1, (self.x24n - 2, self.y + 2))
            screen.blit(sade1, (self.x30n - 2, self.y + 2))
            screen.blit(sade1, (self.x36n - 2, self.y + 2))
            screen.blit(sade1, (self.x42n - 2, self.y + 2))
            screen.blit(sade1, (self.x48n - 2, self.y + 2))


para = par_ammus()
para2 = par_ammus()
para2.nelio2()


class moukari():
    def __init__(self):
        self.kumpin = 1
        self.aloita = 0
        self.luku = 10.  # pitaa alussa olla iso(>=5)
        self.koko_x = 1
        self.r = 40
        self.mones_siirtyma = 11

        self.paikka_x = -1000
        self.paikka_y = -1000

        self.jalka_vaihe = 0

        self.seis_aika = 0

    def nelio2(self):
        self.kumpin = 2

    def suunta(self):  # paina nappia #jalka uudistus
        self.aloita = 0.5

    def suunta2(self):
        if self.aloita == 0.75:

            if self.kumpin == 1:
                yjav.stop()
                self.kohde_x = yjav2.retx() + 10.
                self.kohde_y = yjav2.rety() + 10.

                self.oma_x = yjav.retx() + 10.
                self.oma_y = yjav.rety() + 10.

            elif self.kumpin == 2:
                yjav2.stop()
                self.kohde_x = yjav.retx() + 10.
                self.kohde_y = yjav.rety() + 10.

                self.oma_x = yjav2.retx() + 10.
                self.oma_y = yjav2.rety() + 10.

            self.b = 200. / (math.sqrt((self.oma_x - self.kohde_x) ** 2 + (self.oma_y - self.kohde_y) ** 2))

            self.x0 = (self.oma_x - self.kohde_x) * self.b
            self.y0 = (self.oma_y - self.kohde_y) * self.b

            self.aloita = 1
        else:
            pass

    def koko_aja(self):
        if self.aloita == 0:
            pass
        elif self.aloita == 0.5:  # kunnes jalat on maassa
            if self.kumpin == 1:
                self.oma_a_x = yjav.retx() + 10.  # keksusta
                self.oma_a_y = yjav.rety() + 10.

            elif self.kumpin == 2:
                self.oma_a_x = yjav2.retx() + 10.
                self.oma_a_y = yjav2.rety() + 10.

            self.jalka_vaihe += t1 * 10

            if self.jalka_vaihe <= 4:
                self.jalkaY = jalka1

            elif self.jalka_vaihe <= 8:
                self.jalkaY = jalka2

            elif self.jalka_vaihe <= 12:
                self.jalkaY = jalka3

            elif self.jalka_vaihe <= 16:
                self.jalkaY = jalka4

                self.aloita = 0.75
                self.suunta2()

            screen.blit(self.jalkaY, (int(self.oma_a_x) - 39, int(self.oma_a_y) - 39))


        elif self.aloita == 1:
            self.luku += t1 * 80
            if self.luku >= 5:
                self.koko_x -= 0.1
                self.mones_siirtyma -= 1

                if self.koko_x <= -1:
                    self.koko_x = -1

                vk = 80.
                h = 2.0 - math.sqrt(1.0 - (self.koko_x ** 2))
                self.r = vk / h

                self.paikka_x = self.x0 * (self.mones_siirtyma / 10.)
                self.paikka_y = self.y0 * (self.mones_siirtyma / 10.)

                self.luku = 0

            if self.koko_x <= -1:
                self.maassa()  # aloittaa iskun animaation
                self.aloita = 2  # osuma meneillaan

            if self.aloita != 2:
                screen.blit(jalka4, (int(self.oma_x) - 39, int(self.oma_y) - 39))

                moukari_zoom = pygame.transform.scale(moukari_pallo2, (int(self.r * 2), int(self.r * 2)))
                screen.blit(moukari_zoom,
                            (int(self.oma_x + self.paikka_x - self.r), int(self.oma_y + self.paikka_y - self.r)))



        elif self.aloita == 2:
            pass

    def maassa(self):

        if self.kumpin == 1:

            eta = math.sqrt(((self.oma_x + self.paikka_x) - (yjav2.retx() + 10.)) ** 2 + (
            (self.oma_y + self.paikka_y) - (yjav2.rety() + 10.)) ** 2)
            yjav.anti_stop()
            if eta < 35.5:
                hp2.muuta_hp(-40)
                self.seis_aika = 1
            else:

                self.siivous()


        elif self.kumpin == 2:
            yjav2.anti_stop()
            eta = math.sqrt(((self.oma_x + self.paikka_x) - (yjav.retx() + 10)) ** 2 + (
            (self.oma_y + self.paikka_y) - (yjav.rety() + 10)) ** 2)
            if eta < 35.5:
                hp1.muuta_hp(-40)
                self.seis_aika = 1
            else:
                self.siivous()

    def seis_1(self):  # 2 nelioon on osuttu       #aja koko ajan!!!
        if self.seis_aika == 0:
            pass
        else:
            self.seis_aika += (t1 * 100)
            yjav2.stop()
            self.siivous()
            if self.seis_aika >= 200:
                yjav2.anti_stop()
                self.nollaa_seis_aika()

    def seis_2(self):  # 2 nelioon on osuttu       #aja koko ajan!!!
        if self.seis_aika == 0:
            pass
        else:
            self.seis_aika += (t1 * 100)
            yjav.stop()
            self.siivous()
            if self.seis_aika >= 200:
                yjav.anti_stop()
                self.nollaa_seis_aika()

    def siivous(self):

        self.aloita = 0
        self.luku = 10.  # pitaa alussa olla iso(>=5)
        self.koko_x = 1
        self.r = 40
        self.mones_siirtyma = 11
        self.jalka_vaihe = 0
        if self.kumpin == 1:
            isku1.muuta(0)
        elif self.kumpin == 2:
            isku2.muuta(0)

    def nollaa_seis_aika(self):
        self.seis_aika = 0

    def ret_seis_aika(self):
        return self.seis_aika


moukari1 = moukari()
moukari2 = moukari()
moukari2.nelio2()

# ERI TYYPISET AJAN PAIKAN JA AJAN SKAALAUKSET VOIS YHTENAISTAA
# OHJELMAN SUORITUKSEN AJAN LASKIMEN VOISI YRITTAA LAITTTAA KUNTOON
#
#
#
#
#
#
#
#
#
#






while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

        # 1.pallo
        if event.type == KEYDOWN and hp1.ret_hp() > 0:  # ylos
            # SPACE
            if event.key == K_SPACE and (isku1.ret() == 1 or isku1.ret() == 0):
                isku1.muuta(1)
                objpallot.nro1()
            # Q
            if event.key == K_q and (isku1.ret() == 2 or isku1.ret() == 0):
                isku1.muuta(2)
                if osalama1.energia() > 0 and osalama1.ret_onoff() == 0:
                    salama_vakio = 1

                elif osalama1.energia() == 0 and erik1.ret_bar() >= 1.5:  # or osalama1.ret_onoff == 1
                    osalama1.salama_on()

                else:
                    pass
            # E
            if event.key == K_e and (isku1.ret() == 0) and erik1.ret_bar() >= 50 and para.ret_lahto() == 0:
                isku1.muuta(3)
                erik1.muuta_erik(-50)
                para.nappi_alas()
            # F
            if event.key == K_f and isku1.ret() == 0:
                erik1.muuta_erik(-10)
                moukari1.suunta()
                isku1.muuta(4)

            if event.key == K_w:
                objFFF.akkk(60)
            if event.key == K_s:
                objFFF.akalas(30)
            if event.key == K_a:
                objFFF.akv(50)
            if event.key == K_d:
                objFFF.ako(50)
            if event.key == K_n:
                vaih.lisaa()

        if event.type == KEYUP and hp1.ret_hp() > 0:
            if event.key == K_SPACE:
                isku1.muuta(0)
                objpallot.talletus()
                pp.mones_pallo(1)  # mihin suuntaan lahtee
                pp.alku1()
                pp.laskuja1()

                pp2.mones_pallo(2)
                objpallot.talletus2()
                pp2.alku1()
                pp2.laskuja1()

                pp3.mones_pallo(3)
                objpallot.talletus3()
                pp3.alku1()
                pp3.laskuja1()

                pp4.mones_pallo(4)
                objpallot.talletus4()
                pp4.alku1()
                pp4.laskuja1()

                objpallot.nro2()

            if event.key == K_q:
                isku1.muuta(0)
                osalama1.salama_off()

            if event.key == K_w:
                objFFF.akkk(0)
            if event.key == K_s:
                objFFF.akalas(0)
            if event.key == K_a:
                objFFF.akv(0)
            if event.key == K_d:
                objFFF.ako(0)

        # pallo 2.
        if event.type == KEYDOWN and hp2.ret_hp() > 0:

            if event.key == K_KP0 and (isku2.ret() == 1 or isku2.ret() == 0):
                isku2.muuta(1)
                objpallot2.nro1()

            if event.key == K_KP7 and (isku2.ret() == 2 or isku2.ret() == 0):
                isku2.muuta(2)
                if osalama2.energia() > 0 and osalama2.ret_onoff() == 0:
                    salama_vakio2 = 1


                elif osalama2.energia() == 0 and erik2.ret_bar() >= 1.5:  # or osalama2.ret_onoff == 1
                    osalama2.salama_on()

                else:
                    pass

            if event.key == K_KP9 and (isku2.ret() == 0) and erik2.ret_bar() >= 50 and para2.ret_lahto() == 0:
                isku2.muuta(3)
                para2.nappi_alas()
                erik2.muuta_erik(-50)

            if event.key == K_KP3 and isku1.ret() == 0:
                erik1.muuta_erik(-10)
                moukari2.suunta()
                isku2.muuta(4)

            if event.key == K_KP8:
                objFFF2.akkk(60)
            if event.key == K_KP5:
                objFFF2.akalas(30)
            if event.key == K_KP4:
                objFFF2.akv(50)
            if event.key == K_KP6:
                objFFF2.ako(50)
            if event.key == K_m:
                vaih2.lisaa()

        if event.type == KEYUP and hp2.ret_hp() > 0:

            if event.key == K_KP0:
                isku2.muuta(0)
                objpallot2.talletus()
                p2p.mones_pallo(1)  # mihin suuntaan lahtee
                p2p.alku1()
                p2p.laskuja1()

                p2p2.mones_pallo(2)
                objpallot2.talletus2()
                p2p2.alku1()
                p2p2.laskuja1()

                p2p3.mones_pallo(3)
                objpallot2.talletus3()
                p2p3.alku1()
                p2p3.laskuja1()

                p2p4.mones_pallo(4)
                objpallot2.talletus4()
                p2p4.alku1()
                p2p4.laskuja1()

                objpallot2.nro2()

            if event.key == K_KP7:
                isku2.muuta(0)
                osalama2.salama_off()

            if event.key == K_KP8:
                objFFF2.akkk(0)
            if event.key == K_KP5:
                objFFF2.akalas(0)
            if event.key == K_KP4:
                objFFF2.akv(0)
            if event.key == K_KP6:
                objFFF2.ako(0)

    pl_mi.aja()

    am.ennen_sleep()
    # print str(am.ret_aika())

    if am.ret_aika() >= t1:
        pass
    else:
        time.sleep(t1 - am.ret_aika())  #####

    ##
    ##
    # time.sleep(0.010)
    ##
    ##

    am.jalkeen_sleep()

    erik1.lisaa()
    erik2.lisaa()
    # yla baari nyt vain hp
    screen.lock()
    pygame.draw.rect(tausta, (160, 160, 160, 5), Rect((0, 0), (220, 20)))
    if hp1.ret_hp() > 0:
        pygame.draw.rect(tausta, (231, 9, 9, 5), Rect((10, 10), (hp1.ret_hp() * 1.8, 10)))
    pygame.draw.rect(tausta, (9, 9, 231, 5), Rect((10, 5), (erik1.ret_bar() * 1.8, 5)))

    # toka baari
    pygame.draw.rect(tausta, (160, 160, 160, 5), Rect((600, 0), (220, 20)))
    if hp2.ret_hp() > 0:
        pygame.draw.rect(tausta, (231, 9, 9, 5), Rect((610, 10), (hp2.ret_hp() * 1.8, 10)))
    pygame.draw.rect(tausta, (9, 9, 231, 5), Rect((610, 5), (erik2.ret_bar() * 1.8, 5)))
    screen.unlock()

    tor.aika()
    tor.hae_paikka()
    # palloja
    objpallot.nro3()
    objpallot2.nro3()

    y1 = yjav.yv1()
    x = yjav.retx()

    y2 = yjav2.yv1()
    x2 = yjav2.retx()

    if hp1.ret_hp() <= 0:
        y1 = -1000
        x = -1000

    if hp2.ret_hp() <= 0:
        y2 = -1000
        x2 = -1000

    lp2.aika1001()

    if p2p.ret_matkalla() == 1 or p2p2.ret_matkalla() == 1 or p2p3.ret_matkalla() == 1 or p2p4.ret_matkalla() == 1:
        lp1.hae_ja_laske()

    if pp.ret_matkalla() == 1 or pp2.ret_matkalla() == 1 or pp3.ret_matkalla() == 1 or pp4.ret_matkalla() == 1:
        lp2.hae_ja_laske()

    screen.blit(tausta, (0, 0))
    screen.blit(maat, (0, 560))

    # paraabeli pommi
    para.lentorata()
    para2.lentorata()
    # para.draw_()

    para.ammus_laskut()
    para2.ammus_laskut()
    # paraabeli pommi

    osalama1.salamak()
    osalama2.salamak()

    osalama1.aika()
    osalama2.aika()

    if salama_vakio == 1:
        osalama1.iske()
    if osalama1.energia() == 0:
        salama_vakio = 0

    if salama_vakio2 == 1:
        osalama2.iske()
    if osalama2.energia() == 0:
        salama_vakio2 = 0



        # sahkonelion animaatio
    if sn < 3:
        nelio8 = nelio8_0
    elif sn < 6:
        nelio8 = nelio8_1
    elif sn < 9:
        nelio8 = nelio8_2
    elif sn < 12:
        nelio8 = nelio8_3
    elif sn < 15:
        nelio8 = nelio8_4
    elif sn < 18:
        nelio8 = nelio8_5

    sn += 1
    if sn == 18:
        sn = 0
    # sahkopallon animaatio loppuu


    # nelioden valinta
    if vaih.nell() == 1:
        nelioX = nelio
    elif vaih.nell() == 2:
        nelioX = nelio2
    elif vaih.nell() == 3:
        nelioX = nelio3
    elif vaih.nell() == 4:
        nelioX = nelio4
    elif vaih.nell() == 5:
        nelioX = nelio5
    elif vaih.nell() == 6:
        nelioX = nelio6
    elif vaih.nell() == 7:
        nelioX = nelio7
    elif vaih.nell() == 8:
        nelioX = nelio8
    elif vaih.nell() == 9:
        nelioX = nelio9
    elif vaih.nell() == 10:
        nelioX = nelio10
    elif vaih.nell() == 11:
        nelioX = nelio11

    if vaih2.nell() == 1:
        nelioX2 = nelio
    elif vaih2.nell() == 2:
        nelioX2 = nelio2
    elif vaih2.nell() == 3:
        nelioX2 = nelio3
    elif vaih2.nell() == 4:
        nelioX2 = nelio4
    elif vaih2.nell() == 5:
        nelioX2 = nelio5
    elif vaih2.nell() == 6:
        nelioX2 = nelio6
    elif vaih2.nell() == 7:
        nelioX2 = nelio7
    elif vaih2.nell() == 8:
        nelioX2 = nelio8
    elif vaih2.nell() == 9:
        nelioX2 = nelio9
    elif vaih2.nell() == 10:
        nelioX2 = nelio10
    elif vaih2.nell() == 11:
        nelioX2 = nelio11

    # palloja 1
    X = objpallot.palnro()

    if X == 0:
        pass
    else:
        if X == 1:
            XX = pallo1
            koko = 1
            palloX = pallo1
        elif X == 2:
            palloX = pallo2
            XX = pallo2
            koko = 2
        elif X == 3:
            palloX = pallo3
            XX = pallo3
            koko = 3
        elif X == 4:
            palloX = pallo4
            XX = pallo4
            koko = 4
        elif X == 5:
            palloX = pallo5
            XX = pallo5
            koko = 5
        elif X == 6:
            palloX = pallo6
            XX = pallo6
            koko = 6
        elif X == 7:
            palloX = pallo7
            XX = pallo7
            koko = 7
        elif X == 8:
            palloX = pallo8
            XX = pallo8
            koko = 8
        elif X == 9:
            palloX = pallo9
            XX = pallo9
            koko = 9
        elif X == 10:
            XX = pallo10
            palloX = pallo10
            koko = 10
        ypallo = objpallot.palyk()

        screen.blit(palloX, (x + 35, y1 + ypallo))  # 1
        screen.blit(palloX, (x + ypallo, y1 + 35))  # 2
        screen.blit(palloX, (x - 15 - X * 4, y1 + ypallo))  # 3
        screen.blit(palloX, (x + ypallo, y1 - 15 - 4 * X))  # 4

    tot = pp.totuus1()
    if tot == 0:
        pass
    elif tot == 1:
        pp.paikka1()
        p1x = pp.ret_pallo1_x()
        p1y = pp.ret_pallo1_y()
        screen.blit(XX, (p1x + 35, p1y + ypallo))

    tot2 = pp2.totuus1()
    if tot2 == 0:
        pass
    elif tot2 == 1:
        pp2.paikka1()
        p2x = pp2.ret_pallo1_x()
        p2y = pp2.ret_pallo1_y()
        screen.blit(XX, (p2x + ypallo, p2y + 35))

    tot3 = pp3.totuus1()
    if tot3 == 0:
        pass
    elif tot3 == 1:
        pp3.paikka1()
        p3x = pp3.ret_pallo1_x()
        p3y = pp3.ret_pallo1_y()
        screen.blit(XX, (p3x - 15 - koko * 4, p3y + ypallo))

    tot4 = pp4.totuus1()
    if tot4 == 0:
        pass
    elif tot4 == 1:
        pp4.paikka1()
        p4x = pp4.ret_pallo1_x()
        p4y = pp4.ret_pallo1_y()
        screen.blit(XX, (p4x + ypallo, p4y - 15 - 4 * koko))

    ##2.nelion pallot

    X2 = objpallot2.palnro()

    if X2 == 0:

        pass
    else:
        if X2 == 1:
            XX2 = pallo1
            koko2 = 1
            palloX2 = pallo1
        elif X2 == 2:
            palloX2 = pallo2
            XX2 = pallo2
            koko2 = 2
        elif X2 == 3:
            palloX2 = pallo3
            XX2 = pallo3
            koko2 = 3
        elif X2 == 4:
            palloX2 = pallo4
            XX2 = pallo4
            koko2 = 4
        elif X2 == 5:
            palloX2 = pallo5
            XX2 = pallo5
            koko2 = 5
        elif X2 == 6:
            palloX2 = pallo6
            XX2 = pallo6
            koko2 = 6
        elif X2 == 7:
            palloX2 = pallo7
            XX2 = pallo7
            koko2 = 7
        elif X2 == 8:
            palloX2 = pallo8
            XX2 = pallo8
            koko2 = 8
        elif X2 == 9:
            palloX2 = pallo9
            XX2 = pallo9
            koko2 = 9
        elif X2 == 10:
            XX2 = pallo10
            palloX2 = pallo10
            koko2 = 10
        ypallo2 = objpallot2.palyk()

        screen.blit(palloX2, (x2 + 35, y2 + ypallo2))  # 1
        screen.blit(palloX2, (x2 + ypallo2, y2 + 35))  # 2
        screen.blit(palloX2, (x2 - 15 - X2 * 4, y2 + ypallo2))  # 3
        screen.blit(palloX2, (x2 + ypallo2, y2 - 15 - 4 * X2))  # 4

    ##pallon liike:
    tot2 = p2p.totuus1()
    if tot2 == 0:
        pass
    elif tot2 == 1:
        p2p.paikka1()
        p1x2 = p2p.ret_pallo1_x()
        p1y2 = p2p.ret_pallo1_y()
        screen.blit(XX2, (p1x2 + 35, p1y2 + ypallo2))

    tot22 = p2p2.totuus1()
    if tot22 == 0:
        pass
    elif tot22 == 1:
        p2p2.paikka1()
        p2x2 = p2p2.ret_pallo1_x()
        p2y2 = p2p2.ret_pallo1_y()
        screen.blit(XX2, (p2x2 + ypallo2, p2y2 + 35))

    tot32 = p2p3.totuus1()
    if tot32 == 0:
        pass
    elif tot32 == 1:
        p2p3.paikka1()
        p3x2 = p2p3.ret_pallo1_x()
        p3y2 = p2p3.ret_pallo1_y()
        screen.blit(XX2, (p3x2 - 15 - koko2 * 4, p3y2 + ypallo2))

    tot42 = p2p4.totuus1()
    if tot42 == 0:
        pass
    elif tot42 == 1:
        p2p4.paikka1()
        p4x2 = p2p4.ret_pallo1_x()
        p4y2 = p2p4.ret_pallo1_y()
        screen.blit(XX2, (p4x2 + ypallo2, p4y2 - 15 - 4 * koko2))

    screen.blit(nelioX2, (x2, y2))
    screen.blit(nelioX, (x, y1))
    screen.blit(keha_s, (x2 - 5, y2 - 5))
    screen.blit(keha1, (x - 5, y1 - 5))

    if lp2.aika1001() == 1:  # osuus 2. nelioon
        if ttt == 0:
            rajp2x = x2 + 10
            rajp2y = y2 + 10

        if ttt >= 9:
            rajp2x = -1000
            rajp2y = -1000

        ttt += t1 * 100 / 1.4
        if ttt < 3:
            rajahdX = rajahd3
            rajp2x = x2 + 10
            rajp2y = y2 + 10
        elif ttt < 5 and ttt >= 3:
            rajahdX = rajahd5
            rajp2x = x2 + 10
            rajp2y = y2 + 10
        elif ttt < 7 and ttt >= 5:
            rajahdX = rajahd4
            rajp2x = x2 + 10
            rajp2y = y2 + 10
        elif ttt < 9 and ttt >= 7:
            rajahdX = rajahd1
            rajp2x = x2 + 10
            rajp2y = y2 + 10

        screen.blit(rajahdX, (rajp2x - 820, rajp2y - 580))

        if ttt >= 9:
            ttt = 0
            lp2.nollaa_raj_vakio()

    if lp1.aika1001() == 1:
        if ttt1 == 0:
            rajp1x = x + 10
            rajp1y = y1 + 10

        if ttt1 >= 9:
            rajp1x = -1000
            rajp1y = -1000

        ttt1 += t1 * 100 / 1.4
        if ttt1 < 3:
            rajahdX1 = rajahd3
            rajp1x = x + 10
            rajp1y = y1 + 10
        elif ttt1 < 5 and ttt1 >= 3:
            rajahdX1 = rajahd5
            rajp1x = x + 10
            rajp1y = y1 + 10
        elif ttt1 < 7 and ttt1 >= 5:
            rajahdX1 = rajahd4
            rajp1x = x + 10
            rajp1y = y1 + 10
        elif ttt1 < 9 and ttt1 >= 7:
            rajahdX1 = rajahd1
            rajp1x = x + 10
            rajp1y = y1 + 10

        screen.blit(rajahdX1, (rajp1x - 820, rajp1y - 580))

        if ttt1 >= 9:
            ttt1 = 0
            lp1.nollaa_raj_vakio()

    if tor.ret_aika1() <= 0.02:
        screen.blit(pum4, (tor.ret_valix() - 820, tor.ret_valiy() - 580))
    elif tor.ret_aika1() <= 0.028:
        screen.blit(pum3, (tor.ret_valix() - 820, tor.ret_valiy() - 580))

    moukari1.koko_aja()
    moukari2.koko_aja()

    moukari1.seis_1()
    moukari2.seis_2()

    pygame.display.update()
