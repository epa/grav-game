-r requirements.txt
# cx-Freeze== 5.0, 5.0.1, 5.0.2
# creates No module named __startup__ error
# https://github.com/anthony-tuininga/cx_Freeze/issues/209
cx-Freeze==4.3.4
